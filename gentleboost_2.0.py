import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

data = pd.DataFrame({
    "Имя": ["Данияр", "Дамир", "Фариза", "Жанибек", "Адилет", "Досымжан", "Меиржан","Еркеназ","Нурмахан","Аманжол", "Акнур"],
    "Пол": ["М", "М", "Ж", "М", "М", "М","М", "Ж", "М","М","Ж"],
    "Средний GPA": [3.72, 3.5, 2.8, 3.5, 3.5, 3.77, 2.0, 3.51, 3.0, 2.8, 3.5],
    "Имеет стипендию": [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    "Повышенная стипендия": [1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]  
})

X = data[["Пол", "Средний GPA"]]
y = data["Повышенная стипендия"]

X = pd.get_dummies(X, columns=["Пол"], drop_first=True)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=55)

gentle_boost_model = GradientBoostingClassifier(n_estimators=200, learning_rate=0.05, random_state=42)

gentle_boost_model.fit(X_train, y_train)

y_pred = gentle_boost_model.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy}")